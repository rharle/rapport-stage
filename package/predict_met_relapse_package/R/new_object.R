new_mechanistic_model <- function(col_T, col_Event, col_V, covars_model, coefs, model_function, time_step, t_max, data, description = NULL, saemix_options = saemix::saemixControl()){

    .check_arg(col_T, "character", validate_length)
    .check_arg(col_Event, "character", validate_length)
    .check_arg(col_V, "character", validate_length)
    .check_arg(covars_model, "matrix", validate_covar_model)
    .check_arg(coefs, "MechaModel_Coefs")
    .check_arg(time_step, "numeric")
    .validate(time_step, validate_length)
    .check_arg(t_max, "numeric")
    .validate(t_max, validate_length)
    .check_arg(data, "data.frame", null = TRUE)
    .check_arg(description, "character", null = TRUE)
    .check_arg(saemix_options, "list")
    .check_arg(model_function, "function")


    covars_list <- dimnames(covars_model)[[1]]
    n_covariates <- length(covars_list)

    time_grid <-  seq(0, t_max, time_step)
    if (time_grid[length(time_grid)] != t_max){
        time_grid <- c(time_grid, t_max)
    }

    self <- list(
        data = NULL,
        covariates = covars_list,
        covar_model = covars_model,
        time_step = time_step,
        t_max = t_max,
        time_grid = time_grid,
        description = description,
        coefs = coefs,
        saemix_options = saemix_options,
        ll = NULL,
        bic_covariates = NULL,
        .private =  list(
            n_covariates = n_covariates,
            model_function = model_function,
            col_T = col_T,
            col_Event = col_Event,
            col_V = col_V,
            fitted = FALSE,
            computation_time = NULL,
            conditional_surv = NULL
        )
    )
    class(self) <- "MechaModel"

    self <- setData.MechaModel(self, data)
    return (self)
}

get_structural_columns <- function(x) {
    private_x <- .private(x)
    c(private_x$col_V, private_x$col_T, private_x$col_Event)
}

#' @export
setData.MechaModel <- function(x, data) {
  .check_arg(x, "MechaModel")
  .check_arg(data, "data.frame", null = TRUE)
  .validate(data, validate_data_for_mechaModel, model = x)

  if(!is.null(data))
    data <- tibble::as_tibble(data)

  .set(x, "data") <- data
  .private(x$coefs)$psi_fitted <- NULL
  .private(x$coefs)$psi_map <- NULL
  .private(x)$conditional_surv <- NULL

  return (x)
}

#' @export
coef.MechaModel <- function(object, ...){
    object$coefs
}

#' @export
N.MechaModel <- function(x) {
    if (is.null(x$data)){
        return (0)
    } else {
        return (nrow(x$data))
    }
}

#' @export
`[[.MechaModel` <- function(x, i, exact = TRUE) {
    .check_arg(i, c("numeric", "character"), validate_length)

    if (is.numeric(i)){
        NextMethod()
    } else {
        switch(i,
            map = .private(coef(x))$psi_map,
            NextMethod()
        )
    }
}

#' @export
colnames.MechaModel <- function(x) {
  .check_arg(x, "MechaModel")
  c(
    T = .private(x)[["col_T"]],
    V = .private(x)[["col_V"]],
    Event = .private(x)[["col_Event"]]
  )
}
