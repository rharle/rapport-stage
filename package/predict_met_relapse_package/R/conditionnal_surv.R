#' Mean Conditionnal survival function of the population
#'
#' This computes the conditionnal survival function for a population of patients
#'
#' @section Author:
#' Célestin BIGARRÉ \<\email{celestin.bigarre@@inria.fr}\>
#'
#' @param model a MechanisticModel object
#' @param N_sim the number of bootstrap samples
#' @param parallel should the the computation be performed in parallel
#'                 (with dopar) or not.
#' @return The updated model object
#'
#' @details This computes the conditionnal survival function as :
#' \deqn{S(t) = \frac{1}{N} \Sum_{i = 1}^N S(t \| \psi^i; V_{diag}^i)}
#' with \eqn{\psi^i} an \eqn{V_{diag}^i} sampled from the estimated population
#'
#' @export
conditional_surv.MechaModel <- function(model, N_sim = 500, parallel = TRUE){
  .check_arg(model, "MechaModel")
  if (!is.null(.private(model)$conditional_surv))
        return(invisible (model))
  `%op%` <- if (parallel) `%dorng%` else `%do%`

  V <- model$data[[colnames(model)[["V"]]]]

  cond_surv <- foreach::foreach(i = seq_along(V)) %op% {
    coef_means <- matrix(c(1, as_matrix(model$data[i, model$covariates])), nrow = 1) %*% as_matrix(coef(model))
    eta_sample <- MASS::mvrnorm(N_sim, c(0, 0), as_matrix(coef(model), "omega"))
    coef_sample <- exp(matrix(rep(coef_means, each = N_sim), ncol = 2) + eta_sample)

     TTRs <- cpp_TTR(
      rep.int(V[i], N_sim),
      coef_sample[, 1],
      coef_sample[, 1] / 27.63,
      coef_sample[, 2],
      model[["time_step"]],
      model[["t_max"]]
    )
    S <- sapply(TTRs, Sf, time_grid = model$time_grid, sigma = coef(model)$sigma)
    rm(coef_means, eta_sample, coef_sample, TTRs)
    gc()
    apply(S, 1, mean)
  }

  .private(model)$conditional_surv <- purrr::reduce(cond_surv, rbind)
  return(invisible(model))
}

#' @export
conditional_surv.MechaModel_CV <- function(model, N_sim = 500) {
  .check_arg(model, "MechaModel_CV")

  model_test_list <- .imap_cv(model, ~ setData(.x, test_data(model, .y)))
  .private(model)$conditional_surv <-
    foreach::foreach(i = seq_along(model_test_list)) %dorng% {
      conditional_surv(model_test_list[[i]], N_sim = N_sim, parallel = FALSE)$.private$conditional_surv
    }
    rm(model_test_list)
    gc()
  model[["models_list"]] <- .map_cv(model, conditional_surv, N_sim = N_sim, parallel = FALSE)


  return(model)
}

