#' @export
fit.MechaModel <- function(object, new_data = NULL, silent = TRUE, N_sim_surv = 500){
    .check_arg(object, "MechaModel")
    .check_arg(new_data, "data.frame", null = TRUE)
    .validate(new_data, validate_data_for_mechaModel, model = object)
    .check_arg(silent, "logical")
    saemix_model <- .generate_saemix_model(object)

    col_T <- colnames(object)[["T"]]
    col_V <- colnames(object)[["V"]]
    col_Event <- colnames(object)[["Event"]]

    if (!is.null(new_data)) {
        saemix_data <- .prepare_data_saemix(new_data, col_T, col_Event, col_V,  object$covariates)
    } else if (!is.null(object$data)){
        saemix_data <- .prepare_data_saemix(object[["data"]], col_T, col_Event, col_V, object$covariates)
    } else {
        stop_empty_data()
    }
    if (silent) wrapping_f <- capture.output else wrapping_f <- force
    
    start_time <- Sys.time()
      wrapping_f(
        fit <- saemix::saemix(saemix_model, saemix_data, object$saemix_options)
      )
    end_time <- Sys.time()
    fit_result <- fit["results"]

    object$coefs$pop[["alpha"]] <- fit_result["fixed.psi"][[1]]
    object$coefs$pop[["mu"]]    <- fit_result["fixed.psi"][[2]]
    object$coefs$sigma          <- fit_result["fixed.psi"][[3]]

    object$coefs$omega <- as_matrix(fit_result["omega"][1:2, 1:2])

    beta_covar <- matrix(0, nrow = .private(object)$n_covariates, ncol = 3)
    colnames(beta_covar) <- c("alpha", "mu", "sigma")
    rownames(beta_covar) <- object$covariates
    beta_covar[as.logical(object$covar_model)] <- fit_result["betaC"]
    object$coefs$beta_covar <- beta_covar[, 1:2]
    psi <- exp(as_matrix(fit_result["mean.phi"][, 1:2]))
    colnames(psi) <- c("alpha", "mu")

    if(object$saemix_options$ll.is) {
      object$ll <- fit_result["ll.is"]
      object$bic_covariates <- fit_result["bic.covariate.is"]
    }
    .private(object$coefs)$psi_fitted <- psi
    object$coefs$map.psi <- fit_result["map.psi"]
    object$coefs$map.eta <- fit_result["map.eta"]
    object$coefs$map.phi <- fit_result["map.phi"]

    .private(object)$fitted <- TRUE
    .private(object)$computation_time <- end_time - start_time

    if (N_sim_surv > 0) {
      object <- conditional_surv(object, N_sim = N_sim_surv)
    }

    return(object)
}


.generate_saemix_model <- function(object) {
    .check_arg(object, "MechaModel")

    o <- as.numeric(as_matrix(coef(object), "omega")[1 ,2] != 0)
    saemix::saemixModel(
        .private(object)$model_function,
        description = object$description,
        modeltype = "likelihood",
        transform.par = c(1,1,1),
        psi0 = as_matrix(coef(object), param = "raw_psi"),
        omega.init = as_matrix(coef(object), param = "omega_full"),
        covariance.model = base::matrix(
                                c(1,o,0,
                                  o,1,0,
                                  0,0,0), ncol=3, byrow=TRUE),
        covariate.model = object$covar_model,
        verbose = FALSE
    )
}

#' @export
fit.MechaModel_CV <- function(x, N_sim_surv = 500) {
  .check_arg(x, "MechaModel_CV")
  x$models_list <- .map_cv(x, fit, N_sim_surv = 0)
  .private(x)[["fitted"]] <- TRUE

  if (N_sim_surv > 0) {
    object <- conditional_surv(object, N_sim = N_sim_surv)
  }
  invisible(x)
}


#' @export
bootstrap.MechaModel <- function(x, N = 100, ...){
    .check_arg(x, "MechaModel")
    .check_arg(N, "numeric")
    .validate(N, validate_length)
    .validate(N, validate_range, min = 1)

    col_T <- colnames(x)[["T"]]
    col_V <- colnames(x)[["V"]]
    col_Event <- colnames(x)[["Event"]]

    f <-  function(data, idx){
        saemix_Model <- .generate_saemix_model(x)
        capture.output(
          res <- saemix::saemix(
            saemix_Model,
            .prepare_data_saemix(data[idx, ], col_T, col_Event, col_V, x$covariates),
            x$saemix_options
          )
        )
        res <- res["results"]
        out <- c(res["fixed.effects"], diag(res["omega"])[1:2])
        names(out) <- c(res["name.fixed"], res["name.random"][1:2])
        out
    }
    res <- boot::boot(x$data, statistic = f, R = N, ...)

    return (res)
}