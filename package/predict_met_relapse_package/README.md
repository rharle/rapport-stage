# mechanisticModel R package

Date of Creation : december 18th, 2020

## Description

This package implements a mechanistic model for the prediction of metastatic relapse based on the model proposed by S Benzekry (Modeling spontaneous metastasis following surgery: an in vivo-in silico approach doi:10.1158/0008-5472.CAN-15-1389) and used by chiara Nicolo (Machine Learning and Mechanistic Modeling for Predictionof Metastatic Relapse in Early-Stage Breast Cancer doi:10.1200/CCI.19.00133). This is a new optimized implementation of the model using the numba compiler.

## Authors

 - Célestin BIGARRÉ <celestin.bigarre@inria.fr> [author, maintainer]
 - Sébastien BENZEKRY <sebastien.benzekry@inria.fr> [maintainer]


# Dependencies

## R packages

The following packages are required and will be installed with their own dependencies alongside this package :
  - *reticulate*
  - *saemix* (>= 3.1.9000) from [saemixdevelopment/saemixextension](https://github.com/saemixdevelopment/saemixextension)
  - glue
  - crayon
  - rlist
  - Formula
  - rlang
  - MASS
  - latex2exp
  - generics
  - tibble
  - dplyr
  - purrr

## Python module
The python code is called using the reticulate package. Make sure a python version is available and configured with reticulate before using this package.


The python model relies on the *scipy* and *numba* python modules, make sure they are installed and available for reticulate. Also, the numba compiler needs the extra package *tbb* to be install in order to produce a thread safe version of the code, so please make sure this package is also available through reticulate.

If not installed use the following R command :
```R
reticulate::py_install("scipy")
reticulate::py_install("numba")
reticulate::py_install("tbb")

```

## Usage 
This package relies on a development version of the saemix package available at : <https::github.com/saemixdevelopment/saemixextension>, installing this package should install this new version of saemix but will REMOVE previously installed version of seamix. This developpement version includes the possibility to use a custom logpdf function in the saem algorithm instead of a MLXR model.



## Patch Notes :

    - 1.0.0 : First release of the package, the following functions are available :
        + mechanisticModel : fits the mechanistic model using one of the 3 implementation(original python, c++ reproducing python, c++ optimized)
        + test_saemx_iter : this run the first n iteration of saemix then auit and return internal variables. Tis is a test function that should not be called by a real user.
        + /!\ Note that a lot of documentation id lacking at the time, it will be added in a futur patch
    - 1.1.0 : Major rebase of the source code
        + added a python version optimized with numba

## Done
18/12/2020 :
	- package skeleton
	- added  c++ code reproducing the python model
