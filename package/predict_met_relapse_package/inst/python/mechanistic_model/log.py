import numpy as np

def log_cap(x):
    n   = len(x)
    eps = np.finfo(float).tiny*np.ones(n)
    return np.log(np.maximum(eps,x))