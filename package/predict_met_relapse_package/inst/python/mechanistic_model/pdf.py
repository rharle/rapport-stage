from numpy import float32, array, int32
from mechanistic_model import pre_pdf, h, S, log_cap

def logpdf(
        psi,
        ID,
        xidep,
        event,
        time_vector,
        gamma = 1.0,
        V_vis = float32(65449846.949787356)):
    # converting values for numba
    psi         = array(psi, dtype="float32")
    ID          = int32(ID)
    xidep       = array(xidep, dtype="float32")
    event       = array(event, dtype="int32")
    time_vector = array(time_vector, dtype="float32")
    gamma       = float32(gamma)
    V_vis       = float32(V_vis)

    (TTR_vect, t_vect, h_indexes, S_indexes, sigma_vect) = pre_pdf(
        psi,
        ID,
        xidep,
        event,
        time_vector,
        gamma,
        V_vis)
    h_indexes = h_indexes.astype(bool)
    S_indexes = S_indexes.astype(bool)

    TTR_vect[h_indexes] = log_cap(h(TTR_vect[h_indexes], t_vect[h_indexes], sigma_vect[h_indexes]))
    TTR_vect[S_indexes] = log_cap(S(TTR_vect[S_indexes], t_vect[S_indexes], sigma_vect[S_indexes]))

    return TTR_vect

