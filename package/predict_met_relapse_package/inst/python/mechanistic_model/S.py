from scipy.stats import norm
from numpy import isinf
from mechanistic_model import log_cap
from numpy import ones

def S(TTR_vect, t_vect, sigma_vect):
    if(len(TTR_vect) == 1) and isinf(TTR_vect):
        return ones(len(t_vect))
    
    return norm.sf((log_cap(t_vect) - log_cap(TTR_vect))/sigma_vect)