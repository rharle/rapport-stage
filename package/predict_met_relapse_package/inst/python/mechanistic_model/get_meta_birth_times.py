import numpy as np
from mechanistic_model.TTR import diss
from numba import njit, float32, int32

def get_meta_birth_times_r(V_diag, alpha, beta, mu, time_vector, gamma = 1, V_vis = 65449846.949787356):
    V_diag = np.float32(V_diag)
    alpha = np.float32(alpha)
    beta = np.float32(beta)
    mu = np.float32(mu)
    time_vector = np.float32(time_vector)
    gamma = np.float32(gamma)
    V_vis = np.float32(V_vis)
    if alpha < beta * np.log(V_diag):
        return np.nan
    return get_meta_birth_times(V_diag, alpha, beta, mu, time_vector, gamma, V_vis)

@njit(float32[:](float32, float32, float32, float32, float32[:], float32, float32), cache = True, nogil=True)
def get_meta_birth_times(V_diag, alpha, beta, mu, time_vector, gamma, V_vis):
    t_diag = - np.log(1 - beta * np.log(V_diag)/alpha)/beta
    if gamma == 0:
        nb_meta = np.int32(np.floor(t_diag * mu))
        t_N = np.zeros(nb_meta, dtype = float32)
        for c in range(nb_meta):
            t_N[c] = (c+1) / mu
    else:
        c = int32(1)
        nb_meta = 0
        t_prev = np.float32(0.0)
        t_N = np.zeros(1, dtype=float32)
        time_vector = time_vector[time_vector <= t_diag]
        # time_vector = np.append(time_vector, t_diag)
        for t in time_vector:
            dt = np.float32(t) - t_prev
            nb_meta += dt * (diss(t_prev, alpha, beta, mu, gamma) +
                             diss(t, alpha, beta, mu, gamma)) / 2.0
            if nb_meta >= c:
                t_N = np.append(t_N, np.float32(t))
                c = c + np.int32(1)
            t_prev = t

    return t_N


def get_N_cum_r(V_diag, alpha, beta, mu, time_vector,
                V_vis = 65449846.949787356, gamma = 1):
    V_diag = np.float32(V_diag)
    alpha = np.float32(alpha)
    beta = np.float32(beta)
    mu = np.float32(mu)
    time_vector = np.float32(time_vector)
    V_vis = np.float32(V_vis)
    if alpha < beta * np.log(V_diag):
        return np.nan
    return get_N_cum(V_diag, alpha, beta, mu, time_vector, V_vis, gamma)

@njit(float32[:](float32, float32, float32, float32, float32[:], float32, float32), cache = True, nogil=True)
def get_N_cum(V_diag, alpha, beta, mu, time_vector, V_vis, gamma):
    nb_meta = 0
    c = 1
    t_prev = np.float32(0.0)

    t_diag = - np.log(1 - beta * np.log(V_diag)/alpha)/beta
    time_vector = time_vector[time_vector <= t_diag]
    # time_vector = np.append(time_vector, t_diag)

    N_cum = np.zeros_like(time_vector, dtype=float32)

    for i, t in enumerate(time_vector):
        dt = t - t_prev
        nb_meta += dt * (diss(t_prev, alpha, beta, mu, gamma) + diss(t, alpha, beta, mu, gamma)) / 2.0
        N_cum[i] = nb_meta
        t_prev = t

    return N_cum