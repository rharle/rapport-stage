from scipy.stats import norm
from mechanistic_model import log_cap

def h(TTR_vect, t_vect, sigma_vect):
    return norm.pdf((log_cap(t_vect) - log_cap(TTR_vect)) / sigma_vect) / (t_vect * sigma_vect)
        