import numpy as np
from numba import njit, prange, float32, int32
import config

@njit(float32(float32, float32, float32, float32, float32), cache=True, nogil=True)
def diss(t, alpha, beta, mu, gamma):
    return mu * np.exp(alpha * (1 - np.exp(-beta * t)) / beta) ** gamma

@njit(float32(float32, float32, float32, float32, float32[:], float32, float32), cache = True, nogil=True)
def TTR(V_diag, alpha, beta, mu, time_vector, gamma, V_vis):

    if alpha < beta * np.log(V_diag):
        return np.nan

    t_diag = - np.log(1 - beta * np.log(V_diag)/alpha)/beta
    tau_vis = - np.log(1 - beta * np.log(V_vis)/alpha)/beta
    nb_meta = 0
    t_prev = 0
    t_N = -1
    # cas gamma = 0
    if gamma == 0:
        nb_meta =  mu * t_diag
        t_N = 1 / mu
    # autre cas
    else:
        for t in time_vector:
            if t > t_diag:
                break
            dt = t - t_prev
            nb_meta += dt * (diss(t_prev, alpha, beta, mu, gamma) + diss(t, alpha, beta, mu, gamma)) / 2.0
            if nb_meta >= 1 :
                t_N = t
                break
            t_prev = t

    if nb_meta < 1 :
        return np.inf

    return np.around(t_N + tau_vis - t_diag, 3)

def TTR_outside(V_diag, alpha, beta, mu, time_vector, gamma = 1, V_vis = np.float32(65449846.949787356)):
    V_diag      = np.array(V_diag, dtype = "float32")
    alpha       = np.array(alpha, dtype = "float32")
    beta        = np.array(beta, dtype = "float32")
    mu          = np.array(mu, dtype = "float32")
    gamma       = np.float32(gamma)
    V_vis       = np.float32(V_vis)
    time_vector = np.array(time_vector, dtype = "float32")
    res = np.empty(V_diag.shape[0], dtype = "float32")
    for i in range(V_diag.shape[0]):
        res[i] = TTR(
                V_diag[i],
                alpha[i],
                beta[i],
                mu[i],
                time_vector,
                gamma,
                V_vis
            )
    return res


@njit((float32[:, :], int32[:], float32[:, :], int32[:], float32[:], float32, float32), parallel = config.parallel, nogil = True)
def pre_pdf(psi,
            ID,
            xidep,
            event,
            time_vector,
            gamma,
            V_vis):
    N = len(ID)
    TTR_vect = np.zeros(N, dtype=float32)
    sigma_vect = np.zeros(N, dtype=float32)
    h_indexes = np.zeros(N, dtype=int32)
    S_indexes = np.zeros(N, dtype=int32)
    t_vect = np.zeros(N, dtype=float32)

    for i in prange(N//2):
        j = 2*i + 1
        V_diag = xidep[j, 0]
        T = xidep[j, 1]
        e = event[j]
        alpha = psi[ID[j] - 1, 0]
        beta = alpha/27.63
        mu = psi[ID[j] - 1, 1]
        sigma = psi[ID[j] - 1, 2]

        TTR_vect[j] = TTR(V_diag, alpha, beta, mu, time_vector, gamma, V_vis)
        if np.isnan(TTR_vect[j]):
            TTR_vect[j] = -708.3964185322641

        elif np.isinf(TTR_vect[j]):
            if e:
                TTR_vect[j] = -708.3964185322641
            else:
                TTR_vect[j] = 0

        elif TTR_vect[j] < 0:
            TTR_vect[j] = -708.3964185322641

        else:
            sigma_vect[j] = sigma
            t_vect[j] = T
            if e:
                h_indexes[j] = 1
            else:
                S_indexes[j] = 1

    return (TTR_vect, t_vect, h_indexes, S_indexes, sigma_vect)