#ifndef HELPER_FUNCTIONS_H
#define HELPER_FUNCTIONS_H


struct Params{
	double alpha;
	double beta;
	double mu;
	double sigma;
	double V_diag;
    int time_step;
    int t_max;
    int t_N;
};
typedef double ParametrizedRealFunction(double, Params*);

double integrate_composite_trapezoid(ParametrizedRealFunction* f, int t, Params* p);
double log_cap(double x);
double log1m_cap(double x);
// int min(int x, int y);

#endif