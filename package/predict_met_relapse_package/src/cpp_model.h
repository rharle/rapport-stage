#ifndef CPP_MODEL_H
#define CPP_MODEL_H

#include <Rcpp.h>
#include "common_model_functions.h"

bool is_meta_present_at_diag(Params* p);
double TTR(Params* p);
Rcpp::NumericVector cpp_logpdf(
    Rcpp::NumericMatrix psi,
    Rcpp::NumericVector id,
    Rcpp::NumericMatrix xidep, 
    Rcpp::NumericVector cens,
    Rcpp::NumericVector time_step,
    Rcpp::NumericVector t_max
);


#endif