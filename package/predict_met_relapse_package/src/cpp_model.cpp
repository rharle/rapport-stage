/*
Implements the mechanistic model for metastatic process to be used with seamix R package

Reference : Benzekry et al. Modeling spontaneous metastasis following surgery: An in vivo-in silico approach. Cancer Res 76:535-547, 2016
Author: Celestin BIGARRE
Date : 29/12/2020
*/

#include <Rcpp.h>
#include <cmath>
#include "cpp_model.h"


bool is_meta_present_at_diag(Params* p){
    /*
        Check if the number of metastasis at the time of diagnostic is > 1
        and record the min t that verify this condition       
        Author: Celestin BIGARRE
        Date : 29/12/2020
    */
    double N = 0;

    int stop_time = fmin(t_diag_int(p) +  p->time_step, p->t_max);
    stop_time = fmax(p->time_step + 1, stop_time);
    
    int t = 0;
    for (t = p->time_step; t < stop_time; t += p->time_step){
        N += ((double) p->time_step) * 
            (disseminationFunction(t - p->time_step, p) +
             disseminationFunction(t, p)) / 2.0;
        if (N >=1)
        {
            p->t_N = t;
            return true;
        }
    }
    if (stop_time == p->t_max && t < p->t_max){
        N += ((double) p->t_max - t) *
            (disseminationFunction(t, p) +
             disseminationFunction(p->t_max, p)) / 2.0;
        if (N >=1)
        {
            p->t_N = p->t_max;
            return true;
        }
    }
    return false;
}

double TTR(Params* p){
    /*
        Computes the TTR predicted by the model
        
        Author: Celestin BIGARRE
        Date : 29/12/2020
    */

  return round(1000.0 * (p->t_N +tau_vis_double(p) - t_diag_double(p))) / 1000.0;
}

// [[Rcpp::export]]
Rcpp::NumericVector cpp_logpdf(
  Rcpp::NumericMatrix psi,
  Rcpp::NumericVector id,
  Rcpp::NumericMatrix xidep,
  Rcpp::NumericVector cens,
  Rcpp::NumericVector time_step,
  Rcpp::NumericVector t_max
  ) {
    /*
        This is the main function of the model that computes the logpdf for the non linear  mixed effect model
        INPUTS :
            psi :   N_{patients} x 3 matrix
                    first col is alpha, second col is mu third col is sigma
                    this matrix is given by the saemix call to the model function
            id :    2 * N_{patient} x 1 vector 
                    ith elem contains the index of the psi row corresponding to the ith line of xidep
                    /!\ These indexes start at 1 BE CAREFUL
                    this vector is given by the saemix call to the model function
            xidep : 2 * N_{patient} x (3 + nb_covar) matrix
                    first col is V_diag, second col is time, third col is ignored, rests are covariates not used in the model
                    this matrix is given by the saemix call to the model function
            cens :  2 * N_{patient} x 1 INTEGER vector
                    this vector contains the event status of observations
                    this is the 3rd column of saemix's xidep matrix converted by R to integer
                    if convertion is not done from R, this will be gibberish as 0 in R can be randomly converted as a NaN in c++ 
        RETURN :
            logpdf for the mixed effect model as given by Comets et al. in Parameters estimation in nonlinrear mixed effects
            models using saemix, an R implementation of the SAEM algorithm, J Stat Soft 80:1-41, 2017
        Author: Celestin BIGARRE
        Date : 29/12/2020
    */
    int nrow = id.size();
    int event;
    double t;
    Rcpp::NumericVector result(nrow);
    Params p;
    p.time_step = round(time_step[0]);
    p.t_max = round(t_max[0]);


    for (int i = 0; i < nrow; i++){
        if (! (i % 2)){
            result[i] = 0;
        }
        else {
            p.V_diag = xidep(i, 0);
            t = xidep(i, 1);
            event = round(cens[i]);
            p.alpha = psi(id[i] - 1 , 0);
            p.beta = p.alpha/27.63;
            p.mu = psi(id[i] -1, 1);
            p.sigma = psi(id[i] - 1, 2);
            if (p.alpha < p.beta * log(p.V_diag)){
              result[i] = -708.3964185322641; // log(0+)
            }
            else if (!is_meta_present_at_diag(&p)){
                if (event) {
                  result[i] = -708.3964185322641;
                }
                else {
                  result[i] = 0;
                }
            }
            else {
                double TTR_computed(TTR(&p));
                
                if (TTR_computed < 0)
                  result[i] = -708.3964185322641;
                else if (event) {
                  result[i] = log_cap(h(t, TTR_computed, &p));
                }
                else {
                  result[i] = log_cap(S(t, TTR_computed, &p));
                }
            }
        }
    }
    return result;
}