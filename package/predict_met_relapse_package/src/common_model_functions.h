#ifndef COMMON_MODEL_FUNCTIONS_H
#define COMMON_MODEL_FUNCTIONS_H

#include <Rcpp.h>
#include "helper_functions.h"


const double V_vis =  double(1.0/6.0) * double(3.14159265359) * double(5.0) * double(5.0) * double(5.0) * double(1e6);

double disseminationFunction(double t, Params* p);
int t_diag_int(Params* p);
double t_diag_double(Params* p);
double tau_vis_double(Params* p);
double S(double t, double TTR, Params *p);
double h(double t, double TTR, Params *p);
double pnorm(double x) ;
double dnorm(double x);

#endif