/*


Reference : Benzekry et al. Modeling spontaneous metastasis following surgery: An in vivo-in silico approach. Cancer Res 76:535-547, 2016
Author: Celestin BIGARRE
Date : 18/12/2020
*/

// [[Rcpp::depends(BH)]]
#include <Rcpp.h>
#include <cmath>
#include <iostream>
#include <boost/math/distributions/normal.hpp>
#include "common_model_functions.h"
using boost::math::normal;
// using namespace Rcpp;


double disseminationFunction(double t, Params* p){
	/*
		Computes the dissemination rate at time t given the set of parameters.
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
	double result = p->mu * exp(p->alpha * (1 - exp(-p->beta * t)) / p->beta);
	return result;
}


int t_diag_int(Params* p){
	/*
		Returns the time (as an int) to reach the given primary tumor size according to the model
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
  int result =  round((-log(1 - p->beta * log(p->V_diag) / p->alpha) / p->beta));
  return result;
}

double t_diag_double(Params* p){
	/*
		Returns the time (as a double) to reach the given primary tumor size according to the model
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
  return -log(1 - p->beta * log(p->V_diag) / p->alpha) / p->beta;
}

double tau_vis_double(Params* p){
	/*
		Returns the time (as a double) to reach the visibility threshold according to the model
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
  return -log(1 -  p->beta * log(V_vis) / p->alpha) / p->beta;
}

double S(double t, double TTR, Params *p){
	/*
		Computes the survival function predictes by the model 
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
	normal norm_dist;

	return boost::math::cdf(boost::math::complement(norm_dist, (log_cap(t) - log_cap(TTR)) / p->sigma));
}


double h(double t, double TTR, Params *p){
	/*
		Computes h*S (hazard * Survival) 
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
  normal norm_dist;

	return boost::math::pdf(norm_dist, (log_cap(t) - log_cap(TTR)) / p->sigma) / (p->sigma * t);
}