#include <cmath>
#include "helper_functions.h"

double log_cap(double x){
	/*
		Computes ln function with a minimal value for negative inputs
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
	const double minValue = -708.3964185322641;

	if (x <=  0 ){
		return minValue;
	}
  return fmax(minValue, log(x));
}

double log1m_cap(double x){
	/*
		Computes ln(1-x) function with a minimal value for negative inputs
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
	const double minValue = -708.3964185322641;
	
	if (-x <=  -1 ){
		return minValue;
	}
  return fmax(minValue, log1p(-x));
}

double integrate_composite_trapezoid(ParametrizedRealFunction* f, int t, Params* p){
	/* 
		Computes the integral of the parametrized function f from a to b
		Uses the composite trapezoide rule with a step of 219
		Author: Celestin BIGARRE
		Date : 18/12/2020
	*/
	double I;
	int a, b, dx, N;

	// Initializations
	a = 0;
	dx = p->t_max / 100;
	b = dx * (t / dx);
    N = (b / dx);

    if (t < dx ){
    	return 0.0;
    }

    // integration using composite trapezoidal rule
	I = f(a, p) + f(b, p);
	for (int i = 1; i < N; i++) {
		I += 2 * f(a + i * dx, p);
	}
	I *= dx/2.0;

	return I;
}

// int min(int x, int y){
// 	return x < y ? x : y;
// }

// int max(int x, int y){
// 	return x > y ? x : y;
// }