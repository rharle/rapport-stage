#include <Rcpp.h>
#include <cmath>
#include "cpp_model.h"
#include "common_model_functions.h"

// [[Rcpp::export]]
Rcpp::NumericVector cpp_TTR(
  Rcpp::NumericVector V_diag,
  Rcpp::NumericVector alpha,
  Rcpp::NumericVector beta,
  Rcpp::NumericVector mu,
  Rcpp::NumericVector time_step,
  Rcpp::NumericVector t_max
  ) {
  int nrow = V_diag.size();
  Rcpp::NumericVector result(nrow);
  Params p;
  p.time_step = round(time_step[0]);
  p.t_max = round(t_max[0]);

  for (int i = 0; i < nrow; i++) {
    p.V_diag = V_diag[i];
    p.alpha = alpha[i];
    p.beta = beta[i];
    p.mu = mu[i];
    p.sigma = 0;
    if (p.alpha < p.beta * log(p.V_diag))
      result[i] = R_NaN;
    else if
      (!is_meta_present_at_diag(&p))
      result[i] = R_PosInf;
    else
      result[i] = TTR(&p);
  }

  return(result);
}



// [[Rcpp::export]]
Rcpp::NumericVector cpp_N_cum(
  Rcpp::NumericVector V_diag,
  Rcpp::NumericVector alpha,
  Rcpp::NumericVector beta,
  Rcpp::NumericVector mu,
  Rcpp::NumericVector time_grid
  ) {
  int nrow = time_grid.size();
  Params p;
  p.V_diag = V_diag[0];
  p.alpha = alpha[0];
  p.beta = beta[0];
  p.mu = mu[0];

  if (p.alpha < p.beta * log(p.V_diag))
    return Rcpp::NumericVector::create(R_NaN);

  Rcpp::NumericVector result(nrow);
  result[0] = 0.0;
  double t_prev = 0.0;

  for (int i = 1; i < nrow; i++) {
    double t = time_grid[i];
    result[i] = result[i - 1] + (t - t_prev) *
            (disseminationFunction(t_prev, &p) +
             disseminationFunction(t, &p)) / 2.0;
    t_prev = t;
  }

  return(result);
}


// [[Rcpp::export]]
Rcpp::NumericVector cpp_meta_birth_times(
  Rcpp::NumericVector V_diag,
  Rcpp::NumericVector alpha,
  Rcpp::NumericVector beta,
  Rcpp::NumericVector mu,
  Rcpp::NumericVector time_grid
  ) {
  int nrow(time_grid.size());
  int count(1);
  double n_meta(0.0);
  Params p;
  p.V_diag = V_diag[0];
  p.alpha = alpha[0];
  p.beta = beta[0];
  p.mu = mu[0];

  if (p.alpha < p.beta * log(p.V_diag))
    return Rcpp::NumericVector::create(R_NaN);

  Rcpp::NumericVector result(1);
  result[0] = 0.0;
  double t_prev(0.0);
  double t_max(t_diag_double(&p));
  return Rcpp::NumericVector::create(R_NaN);

  for (int i = 1; i < nrow; i++) {
    double t = time_grid[i];
    if (t >= t_max){
      break;
    }
    n_meta += (t - t_prev) *
              (disseminationFunction(t_prev, &p) +
               disseminationFunction(t, &p)) / 2.0;

    if (n_meta >= count){
      result[i] = count;
      count++;
    }
    // More that 1 onr tumor per time will give incoherent result anyway so return
    if (n_meta >= count) {
      result[i + 1] = R_NaN;
      return (result);
    }
    t_prev = t;
  }

  return(result);
}