// #include <Rcpp.h>
// #include <cmath>
// #include "cpp_model.h"

// using namespace Rcpp;





// // [[Rcpp::export]]
// Rcpp::NumericVector cpp_test_TTR(
//   Rcpp::NumericMatrix psi,
//   Rcpp::NumericVector id,
//   Rcpp::NumericMatrix xidep,
//   Rcpp::NumericVector cens,
//   Rcpp::NumericVector time_step,
//   Rcpp::NumericVector t_max
//   ){
//     /*
//         This is the main function of the model that computes the logpdf for the non linear  mixed effect model
//         INPUTS :
//             psi :   N_{patients} x 3 matrix
//                     first col is alpha, second col is mu third col is sigma
//                     this matrix is given by the saemix call to the model function
//             id :    2 * N_{patient} x 1 vector
//                     ith elem contains the index of the psi row corresponding to the ith line of xidep
//                     /!\ These indexes start at 1 BE CAREFUL
//                     this vector is given by the saemix call to the model function
//             xidep : 2 * N_{patient} x (3 + nb_covar) matrix
//                     first col is V_diag, second col is time, third col is ignored, rests are covariates not used in the model
//                     this matrix is given by the saemix call to the model function
//             cens :  2 * N_{patient} x 1 INTEGER vector
//                     this vector contains the event status of observations
//                     this is the 3rd column of saemix's xidep matrix converted by R to integer
//                     if convertion is not done from R, this will be gibberish as 0 in R can be randomly converted as a NaN in c++
//         RETURN :
//             logpdf for the mixed effect model as given by Comets et al. in Parameters estimation in nonlinrear mixed effects
//             models using saemix, an R implementation of the SAEM algorithm, J Stat Soft 80:1-41, 2017
//         Author: Celestin BIGARRE
//         Date : 29/12/2020
//     */
//     int nrow = id.size();
//     int event;
//     double t;
//     Rcpp::NumericVector result(nrow);
//     Params p;
//     p.time_step = round(time_step[0]);
//     p.t_max = round(t_max[0]);


//     // for (int j = 0; j < nrow / 2; j++){
//     //     int i = 2*j+1;
//     for (int i = 0; i < nrow; i++){
//       if (! (i % 2)){
//           result[i] = 0;
//       }
//       else {
//         p.V_diag = xidep(i, 0);
//         t = xidep(i, 1);
//         event = round(cens[i]);
//         p.alpha = psi(id[i] - 1 , 0);
//         p.beta = p.alpha/27.63;
//         p.mu = psi(id[i] -1, 1);
//         p.sigma = psi(id[i] - 1, 2);
//         if (p.alpha < p.beta * log(p.V_diag))
//           result[i] = R_NaN;
//         else if (!is_meta_present_at_diag(&p))
//           result[i] = R_PosInf;
//         else
//           result[i] = TTR(&p);
//       }
//     }
//     return result;
// }


// // // [[Rcpp::export]]
// // NumericVector cpp_test_N_python_like(
// // 	NumericVector t,
// // 	NumericVector V_diag,
// // 	NumericVector alpha,
// // 	NumericVector beta,
// // 	NumericVector mu,
// // 	NumericVector time_step,
// // 	NumericVector t_max
// // 	){
// // 	/*
// // 		Calls the N computation and is here to be called from R
// // 		Author: Celestin BIGARRE
// // 		Date : 18/12/2020
// // 	*/
// // 	Params p;
// // 	NumericVector result(t.size());
// // 	p.alpha = alpha[0];
// // 	p.beta = beta[0];
// // 	p.mu = mu[0];
// // 	p.V_diag = V_diag[0];
// // 	p.time_step = round(time_step[0]);
// //     p.t_max = round(t_max[0]);

// // 	for (int i = 0; i< t.size(); i++){
// // 		result[i] = N_like_python(t[i], &p);
// // 	}

// // 	return result;
// // }

// // // [[Rcpp::export]]
// // NumericVector cpp_test_TTR_python_like(
// // 	NumericVector V_diag,
// // 	NumericVector alpha,
// // 	NumericVector beta,
// // 	NumericVector mu,
// // 	NumericVector time_step,
// // 	NumericVector t_max
// // 	){
// // 	/*
// // 		Calls the TTR computation and is here to be called from R
// // 		Author: Celestin BIGARRE
// // 		Date : 18/12/2020
// // 	*/
// // 	int nrow(V_diag.size());
// // 	NumericVector result(nrow);
// // 	Params p;
// // 	p.time_step = round(time_step[0]);
// //     p.t_max = round(t_max[0]);

// // 	for (int i = 0; i < nrow; i++){
// // 		p.V_diag = V_diag[i];
// // 		p.alpha = alpha[i];
// // 		p.beta = beta[i];
// // 		p.mu = mu[i];
		
// // 		if (p.alpha < p.beta * log(p.V_diag)){
// // 		  result[i] = R_NaN; // log(0+)
// // 		} else if (N_like_python(p.time_step + t_diag_int(&p) - (t_diag_int(&p) % 219), &p) < 1){
// // 			result[i] = R_PosInf;

// // 		} else {
// // 			result[i] = TTR_like_python(&p);
// // 		}

// // 	}

// // 	return result;
// // }